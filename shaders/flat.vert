#version 330

in vec2 in_Position;
in vec2 in_TexCoord;

out vec2 ex_TexCoord;

uniform vec2 screenSize;
uniform mat4 model;

void main() {
	gl_Position = vec4((model * vec4(in_Position, 0.0, 1.0)).xy / screenSize * 2.0 - 1.0, 0.0, 1.0);
	ex_TexCoord = in_TexCoord;
}
