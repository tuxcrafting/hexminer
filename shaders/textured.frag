#version 330

precision highp float;

in vec2 ex_TexCoord;

uniform sampler2D tex;

uniform vec4 highlight_color;
uniform float highlight_proportion;

void main() {
	gl_FragColor = mix(texture(tex, ex_TexCoord), highlight_color, highlight_proportion);
}
