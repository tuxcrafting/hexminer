#version 330

in vec3 in_Position;
in vec2 in_TexCoord;

out vec2 ex_TexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main() {
	gl_Position = projection * view * model * vec4(in_Position.x, in_Position.y, in_Position.z, 1.0);
	ex_TexCoord = in_TexCoord;
}
