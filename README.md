# Hexminer

A WIP Minecraft clone but with *hexagons*!!

## Dependencies

Most dependencies are included as git submodules but the ones that aren't are:

* OpenGL >=3.2
* GLEW
* SDL 2

## Building

    mkdir build
    cd build
    cmake ..
    make

Should be enough to get you started.

## Controls

* W, A, S, D: Movement
* Space: Jump (or go up in noclip mode)
* B: Go down in noclip mode
* N: Toggle noclip mode
* H: Toggle FPS and coordinates
* Left click: Break a block
* Right click: Place a block
