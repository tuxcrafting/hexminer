/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Hexadecimal coordinates helper.
 */

#pragma once

#include "cglm/cglm.h"

/// The width of a hexagon of radius 1.
#define HEX_WIDTH sqrt(3.0f)
/// The height of a hexagon of radius 1.
#define HEX_HEIGHT 2.0f

/// Offsets of the neighbors of a position.
extern vec3 neighbor_offsets[8];

/**
 * Convert axial coordinates to real coordinates.
 *
 * @param coords The axial coordinates {q, r, y}.
 * @param out The real coordinates {x, y, z}.
 */
static inline void axial_to_real(vec3 coords, vec3 out) {
	vec3 copy;
	glm_vec3_copy(coords, copy);
	out[0] = copy[1] * HEX_WIDTH + copy[0] * HEX_WIDTH / 2.0f;
	out[1] = copy[2];
	out[2] = copy[0] * HEX_HEIGHT * (3.0f / 4.0f);
}

/**
 * Convert real coordinates to axial coordinates.
 *
 * @param coords The real coordinates {x, y, z}.
 * @param out The axial coordinates {q, r, y}.
 */
static inline void real_to_axial(vec3 coords, vec3 out) {
	vec3 copy;
	glm_vec3_copy(coords, copy);
	out[0] = 2.0f / 3.0f * copy[2];
	out[1] = HEX_WIDTH / 3.0f * copy[0] - 1.0f / 3.0f * copy[2];
	out[2] = copy[1];
}
