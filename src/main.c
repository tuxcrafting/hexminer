/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Main file of Hexminer.
 */

#include <stdlib.h>

#include "engine/engine.h"
#include "logging.h"

/**
 * Entry point.
 *
 * @param argc Number of command line arguments.
 * @param argv Command line arguments.
 * @return The exit code.
 */
int main(int argc, char *argv[]) {
	log_msg(LOG_INFO, "hexminer", "Hello, World!");
	engine_init();
	engine_mainloop();
	engine_deinit();
	log_msg(LOG_INFO, "hexminer", "Bye!");
	return EXIT_SUCCESS;
}
