/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Hexadecimal coordinates helper.
 */

#include "hex.h"

vec3 neighbor_offsets[8] = {
	{ 0, -1, 0 },
	{ 1, -1, 0 },
	{ 1, 0, 0 },
	{ 0, 1, 0 },
	{ -1, 1, 0 },
	{ -1, 0, 0 },
	{ 0, 0, 1 },
	{ 0, 0, -1 },
};
