/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Generic hashmap.
 */

#pragma once

#include <stdlib.h>

/// The depth of the hashmap.
#define HASHMAP_DEPTH 8

/// The number of bits in each hashmap.
#define HASHMAP_BITS (32 / HASHMAP_DEPTH)

/// The number of children per hashmap.
#define HASHMAP_CHILDREN (1 << HASHMAP_BITS)

/**
 * A hashmap.
 */
struct hashmap {
	/// The child nodes of a hashmap.
	void *children[HASHMAP_CHILDREN];
};

/**
 * A hashmap entry.
 */
struct hashmap_entry {
	/// The key.
	const char *key;

	/// The value.
	void *value;

	/// The next entry in the list.
	struct hashmap_entry *next;
};

/**
 * Create a hashmap.
 *
 * @return A hashmap.
 */
struct hashmap *hashmap_create();

/**
 * Destroy a hashmap.
 *
 * @param map The hashmap.
 */
void hashmap_destroy(struct hashmap *map);

/**
 * Get the address of the pointer associated to a hash.
 *
 * @param map The hashmap.
 * @param key The key.
 * @param create If nonzero, create new nodes if needed.
 * @param depth The depth of the search.
 * @return The address to the pointer.
 */
void **hashmap_find_pointer(struct hashmap *map, unsigned int key, char create, int depth);

/**
 * Set a value in a hashmap.
 *
 * @param map The hashmap.
 * @param key_n The length of the key.
 * @param key The key.
 * @param value The value.
 */
void hashmap_set(struct hashmap *map, size_t key_n, const char *key, void *value);

/**
 * Get a value in a hashmap.
 *
 * @param map The hashmap.
 * @param key_n The length of the key.
 * @param key The key.
 * @return The value.
 */
void *hashmap_get(struct hashmap *map, size_t key_n, const char *key);

/**
 * Create a 32-bit FNV-1a hash of a string.
 *
 * @param length The length of the data.
 * @param data The data.
 * @return The hash.
 */
unsigned int fnv1a32(size_t length, const char *data);
