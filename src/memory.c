/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Memory helper functions.
 */

#include "memory.h"

#include "logging.h"

void *safe_malloc(size_t size) {
	void *ptr = malloc(size);
	if (ptr == NULL) {
		log_msg(LOG_FATAL, "memory", "Memory allocation failed.");
	}
	return ptr;
}

void *safe_calloc(size_t nmemb, size_t size) {
	void *ptr = calloc(nmemb, size);
	if (ptr == NULL) {
		log_msg(LOG_FATAL, "memory", "Memory allocation failed.");
	}
	return ptr;
}

void safe_free(void *ptr) {
	if (ptr != NULL) {
		free(ptr);
	}
}
