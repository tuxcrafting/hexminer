/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Logging module.
 */

#include "logging.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "engine/engine.h"

/**
 * String representation and color of log_level.
 *
 * @sa log_level
 */
static const char *log_levels[] = {
	"-6DEBUG",
	"--INFO",
	"-3WARNING",
	"-1ERROR",
	"17CRITICAL",
	"17FATAL",
};

void log_msg(enum log_level level, const char *namespace, const char *format, ...) {
	va_list args;
	va_start(args, format);

	const char *level_str = log_levels[level];
	if (level_str[0] != '-') {
		fprintf(stderr, "\033[4%cm", level_str[0]);
	}
	if (level_str[1] != '-') {
		fprintf(stderr, "\033[3%cm", level_str[1]);
	}
	fprintf(stderr, "[%s:%s] ", &level_str[2], namespace);
	vfprintf(stderr, format, args);
	fprintf(stderr, "\033[0m\n");

	if (level == LOG_CRITICAL) {
		engine_deinit();
		exit(EXIT_FAILURE);
	} else if (level == LOG_FATAL) {
		exit(EXIT_FAILURE);
	}

	va_end(args);
}
