/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Logging module.
 */

#pragma once

/**
 * Level of a message.
 */
enum log_level {
/// Debug information.
	LOG_DEBUG = 0,
/// Normal information.
	LOG_INFO,
/// Warning.
	LOG_WARNING,
/// Error.
	LOG_ERROR,
/// Critical error, initiates a soft shutdown.
	LOG_CRITICAL,
/// Fatal error, causes the program to exit immediately.
	LOG_FATAL,
};

/**
 * Log a message.
 *
 * @param level Logging level.
 * @param namespace Name of the module logging the message.
 * @param format Format string, passed to printf.
 * @param ... Parameters for the format string.
 */
void log_msg(enum log_level level, const char *namespace, const char *format, ...);
