/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Generic hashmap.
 */

#include "hashmap.h"

#include <string.h>

#include "memory.h"

struct hashmap *hashmap_create() {
	return safe_calloc(1, sizeof(struct hashmap));
}

void hashmap_destroy(struct hashmap *map) {
	for (int i = 0; i < HASHMAP_CHILDREN; i++) {
		if (map->children[i] != NULL) {
			hashmap_destroy((struct hashmap*)map->children[i]);
		}
	}
	safe_free(map);
}

void **hashmap_find_pointer(struct hashmap *map, unsigned int key, char create, int depth) {
	size_t k = (key >> HASHMAP_BITS * depth) & (HASHMAP_CHILDREN - 1);
	if (depth == 7) {
		return &map->children[k];
	} else {
		if (map->children[k] == NULL) {
			if (create) {
				map->children[k] = hashmap_create();
			} else {
				return NULL;
			}
		}
		return hashmap_find_pointer((struct hashmap*)map->children[k], key, create, depth + 1);
	}
}

void hashmap_set(struct hashmap *map, size_t key_n, const char *key, void *value) {
	unsigned int hash = fnv1a32(key_n, key);
	struct hashmap_entry **ptr = (struct hashmap_entry**)hashmap_find_pointer(map, hash, 1, 0);
	char *key_buffer = safe_malloc(key_n + 1);
	memcpy(key_buffer, key, key_n);
	key_buffer[key_n] = 0;
	while (*ptr != NULL) {
		if (strcmp(key_buffer, (*ptr)->key) == 0) {
			(*ptr)->value = value;
			safe_free(key_buffer);
			return;
		}
		ptr = &(*ptr)->next;
	}

	*ptr = safe_malloc(sizeof(struct hashmap_entry));
	(*ptr)->key = key_buffer;
	(*ptr)->value = value;
	(*ptr)->next = NULL;
}

void *hashmap_get(struct hashmap *map, size_t key_n, const char *key) {
	unsigned int hash = fnv1a32(key_n, key);
	struct hashmap_entry **ptr = (struct hashmap_entry**)hashmap_find_pointer(map, hash, 0, 0);
	if (ptr == NULL) {
		return NULL;
	}
	char *key_buffer = safe_malloc(key_n + 1);
	memcpy(key_buffer, key, key_n);
	key_buffer[key_n] = 0;
	while (*ptr != NULL) {
		if (strcmp(key_buffer, (*ptr)->key) == 0) {
			safe_free(key_buffer);
			return (*ptr)->value;
		}
		ptr = &(*ptr)->next;
	}
	return NULL;
}

unsigned int fnv1a32(size_t length, const char *data) {
	unsigned int hash = 2166136261;
	for (size_t i = 0; i < length; i++) {
		hash ^= data[i];
		hash *= 16777619;
	}
	return hash;
}
