/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * File implementing a bit array.
 */

#pragma once

#include <stdlib.h>

#include "memory.h"

/// A bitarray.
typedef unsigned long *bitarray;

/**
 * Create a bitarray.
 *
 * @param length The number of bits in the array.
 * @return A newly allocated bitarray.
 */
static inline bitarray bitarray_create(size_t length) {
	size_t blength;
	if (length % sizeof(long) != 0) {
		blength = length / sizeof(long) + 1;
	} else {
		blength = length / sizeof(long);
	}
	bitarray array = safe_calloc(blength + 1, sizeof(long));
	array[0] = (long)length;
	return array;
}

/**
 * Destroy a bitarray.
 *
 * @param array The bitarray to free.
 */
static inline void bitarray_destroy(bitarray array) {
	safe_free(array);
}

/**
 * Set a bit in a bitarray.
 *
 * @param array The bitarray.
 * @param index The index.
 * @param value The value, zero or nonzero.
 */
static inline void bitarray_set(bitarray array, size_t index, int value) {
	if (index > array[0]) {
		return;
	}
	if (value) {
		array[index / sizeof(long) + 1] |= 1 << index % sizeof(long);
	} else {
		array[index / sizeof(long) + 1] &= ~(1 << index % sizeof(long));
	}
}

/**
 * Get a bit in a bitarray.
 *
 * @param array The bitarray.
 * @param index The index.
 * @return The value, or -1 if the index is out of bound.
 */
static inline int bitarray_get(bitarray array, size_t index) {
	if (index > array[0]) {
		return -1;
	}
	return (array[index / sizeof(long) + 1] & (1 << index % sizeof(long))) != 0;
}
