/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Model loader.
 */

#include "engine/model.h"

#include <stdio.h>
#include <string.h>

#include "stb_image.h"

#include "hashmap.h"
#include "logging.h"
#include "memory.h"

/*
 * The file format of .h3d files is binary, and made of multiple sections. Numbers are all little-endian.
 * It starts with the magic number, 0x48 0x33 0x44 0x7F, followed by VBO definitions and then data.
 * The VBO definitions start with an 8-bit integer indicating how much VBOs there are, and a padding byte.
 * Follows are the definitions, each in this format:
 *   0- 0  Type.
 *   1- 1  Usage.
 *   2- 2  Data type (only the low 7 bits are used for the data type; the high bit indicates if it is normalized).
 *   3- 3  Elements per row.
 *   4- 5  Stride.
 *   6- 9  Offset in file.
 *  10-13  Length.
 */

/// Lookup table for VBO types.
GLenum vbo_type[] = {
	GL_ARRAY_BUFFER,
	GL_ATOMIC_COUNTER_BUFFER,
	GL_COPY_READ_BUFFER,
	GL_COPY_WRITE_BUFFER,
	GL_DISPATCH_INDIRECT_BUFFER,
	GL_DRAW_INDIRECT_BUFFER,
	GL_ELEMENT_ARRAY_BUFFER,
	GL_PIXEL_PACK_BUFFER,
	GL_PIXEL_UNPACK_BUFFER,
	GL_QUERY_BUFFER,
	GL_SHADER_STORAGE_BUFFER,
	GL_TEXTURE_BUFFER,
	GL_TRANSFORM_FEEDBACK_BUFFER,
	GL_UNIFORM_BUFFER,
};

/// Lookup table for VBO usages.
GLenum vbo_usage[] = {
	GL_STREAM_DRAW, GL_STATIC_DRAW, GL_DYNAMIC_DRAW,
	GL_STREAM_READ, GL_STATIC_READ, GL_DYNAMIC_READ,
	GL_STREAM_COPY, GL_STATIC_COPY, GL_DYNAMIC_COPY,
};

/// Lookup table for VBO datatypes.
GLenum vbo_datatype[] = {
	GL_BYTE, GL_UNSIGNED_BYTE,
	GL_SHORT, GL_UNSIGNED_SHORT,
	GL_INT, GL_UNSIGNED_INT,
	GL_FLOAT, GL_DOUBLE,
};

/// Model cache.
struct hashmap model_cache;

/// Texture cache.
struct hashmap texture_cache;

GLuint model_load(const char *filename) {
	GLuint *m = hashmap_get(&model_cache, strlen(filename), filename);
	if (m != NULL) {
		log_msg(LOG_DEBUG, "engine.model", "Loading %s from cache.", filename);
		return *m;
	}

	log_msg(LOG_DEBUG, "engine.model", "Loading %s.", filename);
	FILE *f = fopen(filename, "rb");
	if (f == NULL) {
		log_msg(LOG_ERROR, "engine.model", "Couldn't open %s for reading.", filename);
		return 0;
	}

	unsigned char buf[32];
	fread(buf, 1, 6, f);
	if (memcmp(buf, "H3D\x7F", 4) != 0) {
		log_msg(LOG_ERROR, "engine.model", "Invalid magic number in %s.", filename);
		fclose(f);
		return 0;
	}

	long vbo_pos = 6;
	int vbo_n = buf[4];

	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GLuint *vbos = safe_calloc(vbo_n, sizeof(GLuint));
	glGenBuffers(4, vbos);

	for (int i = 0; i < vbo_n; i++) {
		fseek(f, vbo_pos, SEEK_SET);
		fread(buf, 1, 14, f);
		vbo_pos += 14;

		GLenum type = vbo_type[buf[0]];
		GLenum usage = vbo_usage[buf[1]];
		GLenum normalized = (buf[2] & 0x80) != 0 ? GL_TRUE : GL_FALSE;
		GLenum datatype = vbo_datatype[buf[2] & 0x7F];
		GLint elm_row = buf[3];
		GLsizei stride = buf[4] + buf[5] * 256;
		long offset = buf[6] + buf[7] * 256 + buf[8] * 65536 + buf[9] * 16777216;
		GLsizeiptr length = buf[10] + buf[11] * 256 + buf[12] * 65536 + buf[13] * 16777216;
		unsigned char *data = safe_malloc(length);

		fseek(f, offset, SEEK_SET);
		fread(data, 1, length, f);

		glBindBuffer(type, vbos[i]);
		glBufferData(type, length, data, usage);

		if (type == GL_ARRAY_BUFFER) {
			glVertexAttribPointer(i, elm_row, datatype, normalized, stride, NULL);
			glEnableVertexAttribArray(i);
		}

		safe_free(data);
	}
	fclose(f);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	m = safe_malloc(sizeof(GLuint));
	*m = vao;
	hashmap_set(&model_cache, strlen(filename), filename, m);
	log_msg(LOG_DEBUG, "engine.model", "%s loaded and cached.", filename);
	return vao;
}

GLuint texture_load_2d(const char *filename) {
	GLuint *m = hashmap_get(&texture_cache, strlen(filename), filename);
	if (m != NULL) {
		log_msg(LOG_DEBUG, "engine.texture", "Loading %s from cache.", filename);
		return *m;
	}

	log_msg(LOG_DEBUG, "engine.texture", "Loading %s.", filename);
	int w, h, ch;
	unsigned char *img = stbi_load(filename, &w, &h, &ch, 0);
	if (img == NULL) {
		log_msg(LOG_ERROR, "engine.texture", "Couldn't read %s.", filename);
		return 0;
	}
	GLuint tex;
	glGenTextures(1, &tex);
	glBindTexture(GL_TEXTURE_2D, tex);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);
	glGenerateMipmap(GL_TEXTURE_2D);

	stbi_image_free(img);
	glBindTexture(GL_TEXTURE_2D, 0);

	m = safe_malloc(sizeof(GLuint));
	*m = tex;
	hashmap_set(&texture_cache, strlen(filename), filename, m);
	log_msg(LOG_DEBUG, "engine.texture", "%s loaded and cached.", filename);
	return tex;
}
