/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Generic interface of the engine.
 */

#pragma once

/**
 * Initialize the engine.
 */
void engine_init();

/**
 * Deinitialize the engine.
 */
void engine_deinit();

/**
 * Engine main loop.
 */
void engine_mainloop();
