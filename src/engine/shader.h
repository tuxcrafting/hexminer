/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Shader submodule of the engine.
 */

#pragma once

#include <stdlib.h>

#include <GL/glew.h>

#include "cglm/cglm.h"

/**
 * Create and compile a shader.
 *
 * @param shader_type Type of the shader.
 * @param name Name displayed in the log.
 * @param filename Filename of the shader source.
 * @return The ID of the shader, or 0 in case of error.
 */
GLuint shader_create(GLenum shader_type, const char *name, const char *filename);

/**
 * Create a shader program.
 *
 * @param shaders_n The number of shaders.
 * @param shaders An array of shaders.
 * @param attribs_n The number of bound attributes.
 * @param attribs The name of bound attributes.
 * @return The ID of the shader program.
 */
GLuint shader_program(size_t shaders_n, GLuint *shaders, size_t attribs_n, const char **attribs);

/**
 * Set a mat4 uniform inside a shader program.
 *
 * @param program Shader program.
 * @param uniform Name of the uniform.
 * @param mat Value of the matrix.
 */
void shader_uniform_mat4f(GLuint program, const char *uniform, mat4 mat);

/**
 * Set a vec4 uniform inside a shader program.
 *
 * @param program Shader program.
 * @param uniform Name of the uniform.
 * @param vec Value of the vector.
 */
void shader_uniform_vec4f(GLuint program, const char *uniform, vec4 vec);

/**
 * Set a vec2 uniform inside a shader program.
 *
 * @param program Shader program.
 * @param uniform Name of the uniform.
 * @param vec Value of the vector.
 */
void shader_uniform_vec2f(GLuint program, const char *uniform, vec2 vec);

/**
 * Set a float uniform inside a shader program.
 *
 * @param program Shader program.
 * @param uniform Name of the uniform.
 * @param value Value of the float.
 */
void shader_uniform_float(GLuint program, const char *uniform, float value);
