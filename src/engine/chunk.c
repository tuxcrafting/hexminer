/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Chunk of the map.
 */

#include "engine/chunk.h"

#include <string.h>

#include "simplexnoise1234.h"

#include "engine/engine_state.h"
#include "engine/render.h"
#include "engine/shader.h"
#include "hex.h"
#include "logging.h"
#include "memory.h"

struct hashmap block_definitions;

struct chunk *chunk_generate(vec3 pos, float seed) {
	vec3 base = {
		pos[0] * CHUNK_Q,
		pos[1] * CHUNK_R,
		pos[2] * CHUNK_Y,
	};
	log_msg(LOG_DEBUG, "engine.chunk", "Generating chunk at {%d, %d, %d}.",
	        (int)pos[0] * CHUNK_Q, (int)pos[1] * CHUNK_R, (int)pos[2] * CHUNK_Y);
	struct chunk *chunk = safe_malloc(sizeof(struct chunk));
	glm_vec3_copy(pos, chunk->position);
	unsigned int hash = fnv1a32(4, "test");
	for (int q = 0; q < CHUNK_Q; q++) {
		for (int r = 0; r < CHUNK_R; r++) {
			float fq = q + base[0], fr = r + base[1];
			float noise = snoise3(fq / 32.0f, fr / 32.0f, seed);
			int inoise = round(noise * CHUNK_Y);
			for (int y = 0; y < CHUNK_Y; y++) {
				chunk->blocks[q][r][y].id = (y + base[2]) <= inoise ? hash : 0;
			}
		}
	}
	return chunk;
}

struct block *chunk_get_block(struct chunk *chunk, vec3 pos) {
	vec3 base = {
		chunk->position[0] * CHUNK_Q,
		chunk->position[1] * CHUNK_R,
		chunk->position[2] * CHUNK_Y,
	};
	vec3 p;
	glm_vec3_sub(pos, base, p);
	if (p[0] < 0 || p[0] >= CHUNK_Q ||
	    p[1] < 0 || p[1] >= CHUNK_R ||
	    p[2] < 0 || p[2] >= CHUNK_Y) {
		return NULL;
	}
	return &chunk->blocks[(int)p[0]][(int)p[1]][(int)p[2]];
}

void chunk_render(struct chunk *chunk) {
	vec3 base = {
		chunk->position[0] * CHUNK_Q,
		chunk->position[1] * CHUNK_R,
		chunk->position[2] * CHUNK_Y,
	};
	for (int q = 0; q < CHUNK_Q; q++) {
		for (int r = 0; r < CHUNK_R; r++) {
			for (int y = 0; y < CHUNK_Y; y++) {
				struct block *block = &chunk->blocks[q][r][y];
				if (block->id == 0) {
					continue;
				}
				vec3 pos = { q, r, y };
				glm_vec3_add(pos, base, pos);
				if (engine_state.pointed && glm_vec3_eqv_eps(pos, engine_state.point_pos)) {
					shader_uniform_float(engine_state.shader_program, "highlight_proportion", 0.5f);
					vec4 white = { 1, 1, 1, 1 };
					shader_uniform_vec4f(engine_state.shader_program, "highlight_color", white);
				}
				chunk_render_block(block, pos);
				shader_uniform_float(engine_state.shader_program, "highlight_proportion", 0.0f);
			}
		}
	}
}

void chunk_render_block(const struct block *block, vec3 pos) {
	for (int i = 0; i < 8; i++) {
		vec3 t;
		glm_vec3_add(pos, neighbor_offsets[i], t);
		struct block *b = map_get_block(&engine_state.map, t);
		if (b == NULL || b->id == 0) {
			goto render_ok;
		}
	}
	return;
render_ok:;
	void **ptr = hashmap_find_pointer(&block_definitions, block->id, 0, 0);
	if (ptr == NULL) {
		return;
	}
	struct block_def *def = (struct block_def*)*ptr;
	vec3 real;
	axial_to_real(pos, real);
	vec3 rot = { 0, 0, 0 };
	vec3 scale = { 1, 1, 1 };
	glBindTexture(GL_TEXTURE_2D, def->texture);
	engine_render_vao(GL_TRIANGLES, def->vao, real, rot, scale);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void block_def_register(const char *id, GLuint vao, GLuint texture) {
	unsigned int nid = fnv1a32(strlen(id), id);
	char *buffer = safe_malloc(strlen(id) + 1);
	void **ptr = hashmap_find_pointer(&block_definitions, nid, 1, 0);
	if (*ptr != NULL) {
		struct block_def *cdef = (struct block_def*)*ptr;
		log_msg(LOG_WARNING, "ID clash between %s and %s. Keeping %s.", cdef->id, id, cdef->id);
	}
	struct block_def *def = safe_malloc(sizeof(struct block_def));
	strcpy(buffer, id);
	def->id = buffer;
	def->vao = vao;
	def->texture = texture;
	*ptr = def;
}
