/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Rendering submodule of the engine.
 */

#pragma once

#include <GL/glew.h>

#include "cglm/cglm.h"

/**
 * Initialize rendering.
 */
void engine_render_init();

/**
 * Prepare and render a frame.
 */
void engine_render_frame();

/**
 * Update the projection matrix.
 */
void engine_render_update_projection();

/**
 * Update the view matrix.
 */
void engine_render_update_view();

/**
 * Render a VAO.
 *
 * @param mode The rendering mode.
 * @param vao The VAO.
 * @param pos The position.
 * @param rot The rotation.
 * @param scale The scale.
 */
void engine_render_vao(GLenum mode, GLuint vao, vec3 pos, vec3 rot, vec3 scale);
