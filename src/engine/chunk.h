/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Chunk of the map.
 */

#pragma once

#include <stdlib.h>

#include <GL/glew.h>

#include "cglm/cglm.h"

#include "hashmap.h"

/// Q-axis length of the chunk.
#define CHUNK_Q 16

/// R-axis length of the chunk.
#define CHUNK_R 16

/// Y-axis length of the chunk.
#define CHUNK_Y 32

/**
 * A block.
 */
struct block {
	/// The ID of the block. Normally a hash.
	/// 0 indicates air.
	unsigned int id;

	/// Used to encode small pieces of data for the block.
	unsigned int data;

	/// Points to larger data.
	void *ldata;
};

/**
 * A 16x16x32 rhombus-shaped chunk of the map.
 */
struct chunk {
	/// The chunk data.
	struct block blocks[CHUNK_Q][CHUNK_R][CHUNK_Y];

	/// The scaled hexagonal position of the chunk.
	vec3 position;
};

/**
 * A block definition.
 */
struct block_def {
	/// The string ID.
	const char *id;
	/// The VAO.
	GLuint vao;
	/// The texture.
	GLuint texture;
};

/// Block definitions.
extern struct hashmap block_definitions;

/**
 * Generate a chunk.
 *
 * @param pos The scaled position.
 * @param seed The seed.
 * @return A chunk.
 */
struct chunk *chunk_generate(vec3 pos, float seed);

/**
 * Get a block somewhere in the chunk.
 *
 * @param chunk The chunk.
 * @param pos The hexagonal position of the block.
 * @return A pointer to the block, or NULL if it is not in this chunk.
 */
struct block *chunk_get_block(struct chunk *chunk, vec3 pos);

/**
 * Render a chunk.
 *
 * @param chunk The chunk.
 */
void chunk_render(struct chunk *chunk);

/**
 * Render a block of a chunk.
 *
 * @param block The block.
 * @param pos The position.
 */
void chunk_render_block(const struct block *block, vec3 pos);

/**
 * Register a block definition.
 *
 * @param id The block ID.
 * @param vao The VAO.
 * @param texture The texture.
 */
void block_def_register(const char *id, GLuint vao, GLuint texture);
