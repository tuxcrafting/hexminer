/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * A map.
 */

#pragma once

#include <string.h>

#include "engine/chunk.h"
#include "hashmap.h"

/**
 * A map.
 */
struct map {
	/// The currently loaded chunks.
	struct hashmap chunks;

	/// The random seed.
	float seed;
};

/**
 * Get (and optionally create) a block in a map.
 *
 * @param map The map.
 * @param pos The block position.
 * @return A pointer to the block.
 */
struct block *map_get_block(struct map *map, vec3 pos);

/**
 * Generate a chunk in a map.
 *
 * @param map The map.
 * @param pos The scaled position.
 * @return The generated chunk.
 */
struct chunk *map_generate_chunk(struct map *map, vec3 pos);

/**
 * Get (and optionally create) a chunk in a map, bypassing the cache.
 *
 * @param map The map.
 * @param pos The scaled position.
 * @return The chunk.
 */
struct chunk *map_get_chunk_direct(struct map *map, vec3 pos);

/**
 * Get (and optionally create) a chunk in a map.
 *
 * @param map The map.
 * @param pos The scaled position.
 * @return The chunk.
 */
struct chunk *map_get_chunk(struct map *map, vec3 pos);

/**
 * Make a chunk position string for the hashmap.
 *
 * @param str The 12-bytes long string buffer.
 * @param pos The scaled position of the chunk.
 */
static inline void map_make_chunk_string(char *str, vec3 pos) {
	union {
		struct {
			int q, r, y;
		} pos;
		char str[12];
	} u;
	u.pos.q = pos[0];
	u.pos.r = pos[1];
	u.pos.y = pos[2];
	memcpy(str, u.str, 12);
}
