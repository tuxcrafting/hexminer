/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Player submodule.
 */

#pragma once

#include "cglm/cglm.h"

/**
 * The movement direction.
 */
enum direction {
	/// Forward.
	DIR_FORWARD = 0,
	/// Backward.
	DIR_BACKWARD,
	/// Left.
	DIR_LEFT,
	/// Right.
	DIR_RIGHT,
	/// Gravity.
	DIR_GRAVITY,
	/// Go down.
	DIR_DOWN,
	/// Jump or go up.
	DIR_UP,
};

/**
 * Move the player.
 *
 * @param direction The direction.
 * @return Zero if the player couldn't move, one otherwise.
 */
char player_move(enum direction direction);

/**
 * Update the player heading.
 *
 * @param xrel X displacement.
 * @param yrel Y displacement.
 */
void player_update_heading(float xrel, float yrel);
