/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Text rendering submodule.
 */

#include "engine/text.h"

#include <GL/glew.h>

#include "font8x8_basic.h"

#include "engine/engine_state.h"
#include "engine/render.h"
#include "engine/shader.h"
#include "logging.h"
#include "memory.h"

/// The font texture.
GLuint font_texture;

/// The font VAO.
GLuint font_vao;

/// The font texture coords VBO.
GLuint font_texcoords;

void text_init() {
	log_msg(LOG_DEBUG, "engine.font", "Loading font.");
	unsigned char *texture_data = safe_calloc(3 * 8 * 8 * 16 * 8, 1);
	for (int i = 0; i < 3 * 8 * 8 * 16 * 8; i++) {
		int pixel_pos = i / 3;
		int row = pixel_pos / (8 * 16);
		int column = pixel_pos % (8 * 16);
		int charx = column / 8;
		int charcol = column % 8;
		int chary = row / 8;
		int charrow = row % 8;
		int chari = charx + chary * 16;
		char *ch = font8x8_basic[chari];
		char n = ch[charrow];
		char b = (n >> charcol) & 1;
		texture_data[i] = b ? 255 : 0;
	}
	glGenTextures(1, &font_texture);
	glBindTexture(GL_TEXTURE_2D, font_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 8 * 16, 8 * 8, 0, GL_RGB, GL_UNSIGNED_BYTE, texture_data);
	glBindTexture(GL_TEXTURE_2D, 0);
	log_msg(LOG_DEBUG, "engine.font", "Font loaded.");
}

void text_draw_character(char ch, mat4 mat) {
	if (ch > 128) {
		ch = '?';
	}
	float char_x = ch % 16;
	float char_y = ch / 16;
	GLfloat texcoords[8];
	// bottom left
	texcoords[0] = char_x / 16;
	texcoords[1] = (char_y + 1) / 8;
	// bottom right
	texcoords[2] = (char_x + 1) / 16;
	texcoords[3] = (char_y + 1) / 8;
	// top left
	texcoords[4] = char_x / 16;
	texcoords[5] = char_y / 8;
	// top right
	texcoords[6] = (char_x + 1) / 16;
	texcoords[7] = char_y / 8;

	shader_uniform_mat4f(engine_state.shader_program_2d, "model", mat);

	glBindBuffer(GL_ARRAY_BUFFER, font_texcoords);
	glBufferData(GL_ARRAY_BUFFER, sizeof(texcoords), texcoords, GL_DYNAMIC_DRAW);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, 0);

	glm_translate_x(mat, 8);
}

void text_draw(const char *text, mat4 mat) {
	mat4 t;
	glm_mat4_copy(mat, t);

	glUseProgram(engine_state.shader_program_2d);
	glBindTexture(GL_TEXTURE_2D, font_texture);

	glGenVertexArrays(1, &font_vao);
	glBindVertexArray(font_vao);

	GLfloat vertices_data[] = {
		0.0f, 0.0f, // bottom left
		8.0f, 0.0f, // bottom right
		0.0f, 8.0f, // top left
		8.0f, 8.0f, // top right
	};
	GLushort indices_data[] = {
		0, 1, 3,
		0, 2, 3,
	};

	GLuint vertices, indices;
	glGenBuffers(1, &vertices);
	glGenBuffers(1, &indices);
	glGenBuffers(1, &font_texcoords);

	glBindBuffer(GL_ARRAY_BUFFER, vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices_data), vertices_data, GL_STREAM_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, font_texcoords);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices_data), indices_data, GL_STREAM_DRAW);

	char c;
	for (;;) {
		c = *text++;
		if (c == '\0') {
			break;
		}
		text_draw_character(c, t);
	}

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteVertexArrays(1, &font_vao);
	glDeleteBuffers(1, &vertices);
	glDeleteBuffers(1, &indices);
	glDeleteBuffers(1, &font_texcoords);
	glUseProgram(engine_state.shader_program);
}
