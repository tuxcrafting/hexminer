/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Text rendering submodule.
 */

#pragma once

#include "cglm/cglm.h"

/**
 * Initialize the text rendering.
 */
void text_init();

/**
 * Draw a single character.
 *
 * @param ch The character.
 * @param mat Transformation matrix to apply.
 */
void text_draw_character(char ch, mat4 mat);

/**
 * Draw a text.
 *
 * @param text String to draw.
 * @param mat Transformation matrix to apply.
 */
void text_draw(const char *text, mat4 mat);
