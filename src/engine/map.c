/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * A map.
 */

#include "engine/map.h"

#include "hex.h"
#include "logging.h"

/// Maximum number of times the cached chunk can miss before it being flushed.
#define MAX_CACHE_MISSES 8

/// Number of times the cached chunk was incorrect.
int cached_chunk_misses;

/// Position of the cached chunk.
vec3 cached_chunk_pos;

/// The cached chunk and its neighbors.
struct chunk *cached_chunk[9];

struct block *map_get_block(struct map *map, vec3 pos) {
	vec3 chunk_pos = {
		floor(pos[0] / CHUNK_Q),
		floor(pos[1] / CHUNK_R),
		floor(pos[2] / CHUNK_Y),
	};
	struct chunk *chunk = map_get_chunk(map, chunk_pos);
	return chunk_get_block(chunk, pos);
}

struct chunk *map_generate_chunk(struct map *map, vec3 pos) {
	char cbuf[12];
	map_make_chunk_string(cbuf, pos);
	struct chunk *ch = chunk_generate(pos, map->seed);
	hashmap_set(&map->chunks, 12, cbuf, ch);
	return ch;
}

struct chunk *map_get_chunk_direct(struct map *map, vec3 pos) {
	char cbuf[12];
	map_make_chunk_string(cbuf, pos);
	struct chunk *ch = hashmap_get(&map->chunks, 12, cbuf);
	if (ch == NULL) {
		return map_generate_chunk(map, pos);
	}
	return ch;
}

struct chunk *map_get_chunk(struct map *map, vec3 pos) {
	vec3 delta;
	glm_vec3_sub(pos, cached_chunk_pos, delta);
	if (cached_chunk[0] != NULL) {
		if (glm_vec3_eq_eps(delta, 0)) {
			cached_chunk_misses = 0;
			return cached_chunk[0];
		}
		for (int i = 0; i < 8; i++) {
			if (glm_vec3_eqv_eps(neighbor_offsets[i], delta)) {
				cached_chunk_misses = 0;
				return cached_chunk[i + 1];
			}
		}
	}
	cached_chunk_misses++;
	if (cached_chunk_misses == MAX_CACHE_MISSES) {
		cached_chunk[0] = NULL;
	}
	struct chunk *chunk = map_get_chunk_direct(map, pos);
	if (cached_chunk[0] == NULL) {
		cached_chunk[0] = chunk;
		glm_vec3_copy(pos, cached_chunk_pos);
		cached_chunk_misses = 0;
		for (int i = 0; i < 8; i++) {
			glm_vec3_add(pos, neighbor_offsets[i], delta);
			cached_chunk[i + 1] = map_get_chunk_direct(map, delta);
		}
	}
	return chunk;
}
