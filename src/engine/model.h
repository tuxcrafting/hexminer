/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Model loader.
 */

#pragma once

#include <GL/glew.h>

/**
 * Load a model from a file and create a VAO. See source code for the format.
 *
 * @param filename The filename.
 * @return A VAO.
 */
GLuint model_load(const char *filename);

/**
 * Load a 2D texture from a file.
 *
 * @param filename The filename.
 * @return A texture.
 */
GLuint texture_load_2d(const char *filename);
