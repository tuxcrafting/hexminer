/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Player submodule.
 */

#include "engine/player.h"

#include "engine/chunk.h"
#include "engine/engine_state.h"
#include "engine/render.h"
#include "hex.h"
#include "logging.h"

char player_move(enum direction direction) {
	vec3 new_pos, heading;
	vec3 t;
	glm_vec3_copy(engine_state.player_heading, heading);
	heading[1] = 0;
	glm_vec3_normalize(heading);
	switch (direction) {
	case DIR_FORWARD:
		glm_vec3_scale(heading, engine_state.player_speed, t);
		glm_vec3_add(engine_state.player_pos, t, new_pos);
		break;
	case DIR_BACKWARD:
		glm_vec3_scale(heading, engine_state.player_speed, t);
		glm_vec3_sub(engine_state.player_pos, t, new_pos);
		break;
	case DIR_LEFT:
		glm_vec3_cross(heading, engine_state.view_up, t);
		glm_vec3_normalize(t);
		glm_vec3_scale(t, engine_state.player_speed, t);
		glm_vec3_sub(engine_state.player_pos, t, new_pos);
		break;
	case DIR_RIGHT:
		glm_vec3_cross(heading, engine_state.view_up, t);
		glm_vec3_normalize(t);
		glm_vec3_scale(t, engine_state.player_speed, t);
		glm_vec3_add(engine_state.player_pos, t, new_pos);
		break;
	case DIR_GRAVITY:
		if (engine_state.noclip) {
			engine_state.player_vspeed = 0;
		}
		glm_vec3_copy(engine_state.player_pos, new_pos);
		new_pos[1] += engine_state.player_vspeed;
		break;
	case DIR_DOWN:
		if (engine_state.noclip) {
			glm_vec3_copy(engine_state.player_pos, new_pos);
			new_pos[1] -= engine_state.player_speed;
		}
		break;
	case DIR_UP:
		if (engine_state.noclip) {
			glm_vec3_copy(engine_state.player_pos, new_pos);
			new_pos[1] += engine_state.player_speed;
			break;
		}
		engine_state.player_vspeed += engine_state.player_jump_accel;
		player_move(DIR_GRAVITY);
		break;
	}
	if (!engine_state.noclip) {
		real_to_axial(new_pos, t);
		t[0] = round(t[0]);
		t[1] = round(t[1]);
		t[2] = round(t[2]) - 3;
		for (int i = 0; i < 4; i++) {
			struct block *block = map_get_block(&engine_state.map, t);
			if (block != NULL && block->id != 0) {
				if (direction == DIR_GRAVITY) {
					engine_state.on_ground = 1;
					engine_state.player_vspeed = 0;
				}
				return 0;
			}
			t[2]++;
		}
	}
	glm_vec3_copy(new_pos, engine_state.player_pos);
	engine_render_update_view();
	return 1;
}

void player_update_heading(float xrel, float yrel) {
	vec3 front;
	engine_state.yaw += xrel * engine_state.mouse_sensibility;
	engine_state.pitch -= yrel * engine_state.mouse_sensibility;
	if (engine_state.pitch > 89.0f) {
		engine_state.pitch = 89.0f;
	}
	if (engine_state.pitch < -89.0f) {
		engine_state.pitch = -89.0f;
	}
	front[0] =
		cos(glm_rad(engine_state.pitch)) *
		cos(glm_rad(engine_state.yaw));
	front[1] =
		sin(glm_rad(engine_state.pitch));
	front[2] =
		cos(glm_rad(engine_state.pitch)) *
		sin(glm_rad(engine_state.yaw));
	glm_normalize(front);
	glm_vec3_copy(front, engine_state.player_heading);
	engine_render_update_view();
}
