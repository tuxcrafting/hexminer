/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Global state of the engine.
 */

#pragma once

#include <SDL2/SDL.h>

#include <GL/glew.h>

#include "cglm/cglm.h"

#include "bitarray.h"
#include "engine/map.h"

/**
 * Struct containing the global state of the engine.
 */
struct engine_state {
	/* Flags */

	/// Set if SDL has been initialized.
	unsigned int sdl_initialized : 1;

	/// Set to stop the mainloop.
	unsigned int stop : 1;

	/// Is a block pointed to?
	unsigned int pointed : 1;

	/// Debug messages enabled.
	unsigned int debug : 1;

	/// Noclip enabled.
	unsigned int noclip : 1;

	/// Player is on ground.
	unsigned int on_ground : 1;

	/* SDL and OpenGL */

	/// SDL window.
	SDL_Window *window;

	/// SDL OpenGL context.
	SDL_GLContext gl_context;

	/// Player position.
	vec3 player_pos;

	/// 3D shader program.
	GLuint shader_program;

	/// 2D shader program.
	GLuint shader_program_2d;

	/// The pressed keys.
	bitarray pressed_keys;

	/* Player and camera */

	/// Player heading.
	vec3 player_heading;

	/// Player speed.
	float player_speed;

	/// Player vertical speed.
	float player_vspeed;

	/// Player jump acceleration.
	float player_jump_accel;

	/// View matrix up vector.
	vec3 view_up;

	/// Camera yaw.
	float yaw;

	/// Camera pitch.
	float pitch;

	/* Settings */

	/// Mouse sensibility.
	float mouse_sensibility;

	/* Map */

	/// The map.
	struct map map;

	/// The pointed-to position.
	vec3 point_pos;

	/// The position before the pointed-to position.
	vec3 point_pos_before;

	/// The gravitational acceleration.
	float gravity;
};

/// Global state of the engine.
extern struct engine_state engine_state;
