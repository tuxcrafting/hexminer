/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Rendering submodule of the engine.
 */

#include "engine/render.h"

#include <SDL2/SDL.h>

#include "engine/engine_state.h"
#include "engine/model.h"
#include "engine/player.h"
#include "engine/shader.h"
#include "engine/text.h"

#include "hex.h"
#include "logging.h"

void GLAPIENTRY gl_callback(GLenum source,
                            GLenum type,
                            GLuint id,
                            GLenum severity,
                            GLsizei length,
                            const GLchar* message,
                            const void* userParam) {
	log_msg(type == GL_DEBUG_TYPE_ERROR ? LOG_ERROR : LOG_DEBUG,
	        "opengl", "GL CALLBACK: type = 0x%x, severity = 0x%x, message = %s",
	        type, severity, message);
}

void engine_render_init() {
	engine_state.debug = 1;
	engine_state.player_speed = 0.2f;
	engine_state.player_jump_accel = 0.25f;
	engine_state.mouse_sensibility = 0.05f;
	engine_state.gravity = 0.01f;

	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
	glDebugMessageCallback(gl_callback, 0);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	log_msg(LOG_DEBUG, "engine.render", "Loading shaders.");
	GLuint shaders[4] = {
		shader_create(GL_VERTEX_SHADER, "projection.vert", "shaders/projection.vert"),
		shader_create(GL_FRAGMENT_SHADER, "textured.frag", "shaders/textured.frag"),
		shader_create(GL_VERTEX_SHADER, "flat.vert", "shaders/flat.vert"),
		0,
	};
	shaders[3] = shaders[1];
	const char *attribs[] = {
		"in_Position",
		"in_TexCoord",
	};
	engine_state.shader_program = shader_program(2, shaders, 2, attribs);
	engine_state.shader_program_2d = shader_program(2, &shaders[2], 2, attribs);

	glUseProgram(engine_state.shader_program);

	glDeleteShader(shaders[0]);
	glDeleteShader(shaders[1]);
	glDeleteShader(shaders[2]);

	GLuint hex = model_load("models/hexagon.h3d");
	GLuint tex = texture_load_2d("textures/template.png");

	block_def_register("test", hex, tex);

	engine_state.player_pos[1] = CHUNK_Y + 10;
	engine_state.view_up[1] = 1;
	player_update_heading(0, 0);

	engine_render_update_projection();
	engine_render_update_view();

	text_init();
}

void engine_render_frame() {
	static unsigned int frame_count;
	static unsigned int frame_time;
	static unsigned int last_ticks;

	glClearColor(0.4, 0.7, 0.9, 1.0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	engine_state.pointed = 0;
	vec3 target;
	glm_vec3_scale(engine_state.player_heading, 10, target);
	glm_vec3_add(target, engine_state.player_pos, target);
	glm_vec3_copy(engine_state.player_pos, engine_state.point_pos_before);
	for (int i = 0; i <= 15; i++) {
		float n = (float)i / 15.0f;
		vec3 lerp;
		glm_vec3_lerp(engine_state.player_pos, target, n, lerp);
		vec3 hex;
		real_to_axial(lerp, hex);
		hex[0] = round(hex[0]);
		hex[1] = round(hex[1]);
		hex[2] = round(hex[2]);
		struct block *block = map_get_block(&engine_state.map, hex);
		if (block != NULL && block->id != 0) {
			glm_vec3_copy(hex, engine_state.point_pos);
			engine_state.pointed = 1;
			break;
		}
		glm_vec3_copy(hex, engine_state.point_pos_before);
	}

	vec3 chunk_pos;
	real_to_axial(engine_state.player_pos, chunk_pos);
	int chunk_q = round(chunk_pos[0] / CHUNK_Q);
	int chunk_r = round(chunk_pos[1] / CHUNK_R);
	int chunk_y = round(chunk_pos[2] / CHUNK_Y);
	for (int q = chunk_q - 1; q < chunk_q + 1; q++) {
		for (int r = chunk_r - 1; r < chunk_r + 1; r++) {
			for (int y = chunk_y - 1; y < chunk_y + 1; y++) {
				chunk_pos[0] = q;
				chunk_pos[1] = r;
				chunk_pos[2] = y;
				struct chunk *chunk = map_get_chunk(&engine_state.map, chunk_pos);
				chunk_render(chunk);
			}
		}
	}

	vec3 vec;
	mat4 mat;
	char buf[128];
	vec[0] = 3;
	vec[1] = 3;
	glm_scale_make(mat, vec);

	if (frame_count != 0) {
		if (engine_state.debug) {
			sprintf(buf, "Avg FPS: %.2f", 1000.0f / ((float)frame_time / frame_count));
			text_draw(buf, mat);
		}
	}
	if (frame_count == 120) {
		frame_count = 0;
		frame_time = 0;
	}
	if (engine_state.debug) {
		vec3 t;
		real_to_axial(engine_state.player_pos, t);
		sprintf(buf, "Q=%.2f R=%.2f Y=%.2f",
		        t[0],
		        t[1],
		        t[2]);
		glm_translate_y(mat, 8);
		text_draw(buf, mat);
	}

	SDL_GL_SwapWindow(engine_state.window);

	frame_time += SDL_GetTicks() - last_ticks;
	frame_count++;
	last_ticks = SDL_GetTicks();
}

void engine_render_update_projection() {
	mat4 proj;
	int w, h;
	SDL_GetWindowSize(engine_state.window, &w, &h);
	glViewport(0, 0, w, h);
	glm_perspective(glm_rad(45.0f), (float)w / (float)h, 0.1f, 100.0f, proj);
	shader_uniform_mat4f(engine_state.shader_program, "projection", proj);
	vec2 screen = { w, h };
	glUseProgram(engine_state.shader_program_2d);
	shader_uniform_vec2f(engine_state.shader_program_2d, "screenSize", screen);
	glUseProgram(engine_state.shader_program);
}

void engine_render_update_view() {
	mat4 view;
	glm_look(engine_state.player_pos, engine_state.player_heading, engine_state.view_up, view);
	shader_uniform_mat4f(engine_state.shader_program, "view", view);
}

void engine_render_vao(GLenum mode, GLuint vao, vec3 pos, vec3 rot, vec3 scale) {
	mat4 model;
	glm_mat4_identity(model);
	glm_scale(model, scale);
	glm_rotate_z(model, rot[2], model);
	glm_rotate_y(model, rot[1], model);
	glm_rotate_x(model, rot[0], model);
	glm_translate(model, pos);
	shader_uniform_mat4f(engine_state.shader_program, "model", model);

	glBindVertexArray(vao);
	int size;
	glGetBufferParameteriv(GL_ELEMENT_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
	glDrawElements(mode, size / sizeof(GLushort), GL_UNSIGNED_SHORT, 0);
	glBindVertexArray(0);
}
