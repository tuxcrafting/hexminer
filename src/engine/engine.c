/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Generic interface of the engine.
 */

#include "engine/engine.h"

#include <stdlib.h>

#include "engine/engine_state.h"
#include "engine/player.h"
#include "engine/render.h"
#include "hex.h"
#include "logging.h"

struct engine_state engine_state;

void engine_init() {
	glewExperimental = GL_TRUE;
	engine_state.pressed_keys = bitarray_create(256);

	log_msg(LOG_INFO, "engine", "Initializing...");

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		log_msg(LOG_CRITICAL, "engine", "SDL initialization failed: %s.", SDL_GetError());
	}
	engine_state.sdl_initialized = 1;
	log_msg(LOG_DEBUG, "engine", "SDL initialized.");

	engine_state.window = SDL_CreateWindow(
		"Hexminer",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		512,
		512,
		SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
	if (engine_state.window == NULL) {
		log_msg(LOG_CRITICAL, "engine", "Window creation failed: %s.", SDL_GetError());
	}
	log_msg(LOG_DEBUG, "engine", "Window created.");

#define SET_GL_ATTR(attr, value)	  \
	if (SDL_GL_SetAttribute(attr, value) < 0) { \
		log_msg(LOG_CRITICAL, "Setting GL attribute " #attr " to " #value "failed: %s.", SDL_GetError()); \
	}

	SET_GL_ATTR(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SET_GL_ATTR(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SET_GL_ATTR(SDL_GL_CONTEXT_MINOR_VERSION, 2);
	SET_GL_ATTR(SDL_GL_DOUBLEBUFFER, 1);

#undef SET_GL_ATTR

	log_msg(LOG_DEBUG, "engine", "Successfully set GL attributes.");

	engine_state.gl_context = SDL_GL_CreateContext(engine_state.window);
	if (engine_state.gl_context == NULL) {
		log_msg(LOG_CRITICAL, "engine", "GL context creation failed: %s.", SDL_GetError());
	}
	log_msg(LOG_DEBUG, "engine", "GL context created.");

	if (glewInit() != GLEW_OK) {
		log_msg(LOG_CRITICAL, "engine", "GLEW initialization failed.");
	}
	log_msg(LOG_DEBUG, "engine", "GLEW initialized.");

	if (SDL_GL_SetSwapInterval(-1) < 0) {
		log_msg(LOG_DEBUG, "engine", "Adaptive VSYNC isn't supported (%s); trying with normal VSYNC.", SDL_GetError());
		if (SDL_GL_SetSwapInterval(1) < 0) {
			log_msg(LOG_CRITICAL, "engine", "Normal VSYNC isn't supported (%s).", SDL_GetError());
		} else {
			log_msg(LOG_DEBUG, "engine", "Framerate synchronized with normal VSYNC.");
		}
	} else {
		log_msg(LOG_DEBUG, "engine", "Framerate synchronized with adaptive VSYNC.");
	}
}

void engine_deinit() {
	if (!engine_state.stop) {
		log_msg(LOG_WARNING, "engine", "Deinitialization wasn't called cleanly.");
	}
	log_msg(LOG_INFO, "engine", "Deinitializing...");

	if (engine_state.gl_context != NULL) {
		SDL_GL_DeleteContext(engine_state.gl_context);
		log_msg(LOG_DEBUG, "engine", "GL context deleted.");
	}

	if (engine_state.window != NULL) {
		SDL_DestroyWindow(engine_state.window);
		log_msg(LOG_DEBUG, "engine", "Window destroyed.");
	}

	if (engine_state.sdl_initialized) {
		SDL_Quit();
		log_msg(LOG_DEBUG, "engine", "SDL deinitialized.");
	}
}

void engine_mainloop() {
	log_msg(LOG_DEBUG, "engine", "Mainloop entered.");
	engine_render_init();
	int capture = SDL_FALSE;
	while (!engine_state.stop) {
		SDL_Event event;
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				engine_state.stop = 1;
				break;
			case SDL_KEYDOWN:
				bitarray_set(engine_state.pressed_keys, event.key.keysym.sym, 1);
				switch (event.key.keysym.sym) {
				case SDLK_c:
					capture ^= SDL_TRUE;
					SDL_SetRelativeMouseMode(capture);
					break;
				case SDLK_h:
					engine_state.debug ^= 1;
					break;
				case SDLK_n:
					engine_state.noclip ^= 1;
					break;
				}
				break;
			case SDL_KEYUP:
				bitarray_set(engine_state.pressed_keys, event.key.keysym.sym, 0);
				break;
			case SDL_MOUSEMOTION:
				if (capture) {
					player_update_heading(event.motion.xrel, event.motion.yrel);
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (engine_state.pointed) {
					switch (event.button.button) {
					case SDL_BUTTON_LEFT:
						map_get_block(&engine_state.map, engine_state.point_pos)->id = 0;
						break;
					case SDL_BUTTON_RIGHT:
						map_get_block(&engine_state.map, engine_state.point_pos_before)->id =
							fnv1a32(4, "test");
						break;
					}
				}
				break;
			case SDL_WINDOWEVENT:
				switch (event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					engine_render_update_projection();
					break;
				}
				break;
			}
		}
		engine_state.on_ground = 0;
		engine_state.player_vspeed -= engine_state.gravity;
		player_move(DIR_GRAVITY);
		if (bitarray_get(engine_state.pressed_keys, SDLK_w)) {
			player_move(DIR_FORWARD);
		}
		if (bitarray_get(engine_state.pressed_keys, SDLK_s)) {
			player_move(DIR_BACKWARD);
		}
		if (bitarray_get(engine_state.pressed_keys, SDLK_a)) {
			player_move(DIR_LEFT);
		}
		if (bitarray_get(engine_state.pressed_keys, SDLK_d)) {
			player_move(DIR_RIGHT);
		}
		if (bitarray_get(engine_state.pressed_keys, SDLK_SPACE)
		    && (engine_state.on_ground || engine_state.noclip)) {
			player_move(DIR_UP);
		}
		if (bitarray_get(engine_state.pressed_keys, SDLK_b)) {
			player_move(DIR_DOWN);
		}
		engine_render_frame();
	}
}
