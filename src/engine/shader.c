/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Shader submodule of the engine.
 */

#include "engine/shader.h"

#include <stdio.h>

#include "logging.h"
#include "memory.h"

GLuint shader_create(GLenum shader_type, const char *name, const char *filename) {
	log_msg(LOG_DEBUG, "engine.shader", "Compiling shader %s.", name);
	GLuint shader = glCreateShader(shader_type);
	if (shader == 0) {
		log_msg(LOG_ERROR, "engine.shader", "Error creating shader %s (Error %04X).", name, glGetError());
		return 0;
	}

	FILE *f = fopen(filename, "r");
	if (f == NULL) {
		log_msg(LOG_ERROR, "engine.shader", "Can't open %s's source %s for reading.", name, filename);
		return 0;
	}

	fseek(f, 0, SEEK_END);
	long size = ftell(f);
	char *buf = safe_malloc(size);
	fseek(f, 0, SEEK_SET);
	fread(buf, size, 1, f);
	fclose(f);

	const GLchar *sources[] = { (GLchar*)buf };
	const GLint lengths[] = { (GLint)size };
	glShaderSource(shader, 1, sources, lengths);
	safe_free(buf);

	glCompileShader(shader);
	GLint params;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &params);

	if (params == GL_FALSE) {
		glDeleteShader(shader);
		log_msg(LOG_ERROR, "engine.shader", "Compilation of shader %s failed.", name);
		return 0;
	}

	log_msg(LOG_DEBUG, "engine.shader", "Compiled shader %s.", name);
	return shader;
}

GLuint shader_program(size_t shaders_n, GLuint *shaders, size_t attribs_n, const char **attribs) {
	GLuint program = glCreateProgram();
	if (program == 0) {
		log_msg(LOG_ERROR, "engine.shader", "Error creating shader program.");
		return 0;
	}
	for (size_t i = 0; i < shaders_n; i++) {
		glAttachShader(program, shaders[i]);
	}
	for (size_t i = 0; i < attribs_n; i++) {
		glBindAttribLocation(program, i, attribs[i]);
	}
	glLinkProgram(program);
	return program;
}

void shader_uniform_mat4f(GLuint program, const char *uniform, mat4 mat) {
	glUniformMatrix4fv(glGetUniformLocation(program, uniform),
	                   1, GL_FALSE, &mat[0][0]);
}

void shader_uniform_vec4f(GLuint program, const char *uniform, vec4 vec) {
	glUniform4fv(glGetUniformLocation(program, uniform),
	             1, &vec[0]);
}

void shader_uniform_vec2f(GLuint program, const char *uniform, vec2 vec) {
	glUniform2fv(glGetUniformLocation(program, uniform),
	             1, &vec[0]);
}

void shader_uniform_float(GLuint program, const char *uniform, float value) {
	glUniform1fv(glGetUniformLocation(program, uniform),
	             1, &value);
}
