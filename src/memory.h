/**
 * @file
 * @author tuxcrafting
 * @version 1.0
 *
 * Memory helper functions.
 */

#pragma once

#include <stdlib.h>

/**
 * Similar to malloc(), but exits on failure.
 *
 * @param size Size of the allocation.
 * @return A pointer to allocated memory.
 */
void *safe_malloc(size_t size);

/**
 * Similar to calloc(), but exits on failure.
 *
 * @param nmemb Number of items to be allocated.
 * @param size Size of an item.
 * @return A pointer to allocated memory.
 */
void *safe_calloc(size_t nmemb, size_t size);

/**
 * Similar to free(), but ignored null pointers.
 *
 * @param ptr Pointer to allocated memory.
 */
void safe_free(void *ptr);
