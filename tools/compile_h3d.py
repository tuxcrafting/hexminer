# compile a H3S file into a H3D. usage: compile_h3d.py < file.h3s > file.h3d

import sys
import struct
import math

types = [
	"GL_ARRAY_BUFFER",
	"GL_ATOMIC_COUNTER_BUFFER",
	"GL_COPY_READ_BUFFER",
	"GL_COPY_WRITE_BUFFER",
	"GL_DISPATCH_INDIRECT_BUFFER",
	"GL_DRAW_INDIRECT_BUFFER",
	"GL_ELEMENT_ARRAY_BUFFER",
	"GL_PIXEL_PACK_BUFFER",
	"GL_PIXEL_UNPACK_BUFFER",
	"GL_QUERY_BUFFER",
	"GL_SHADER_STORAGE_BUFFER",
	"GL_TEXTURE_BUFFER",
	"GL_TRANSFORM_FEEDBACK_BUFFER",
	"GL_UNIFORM_BUFFER",
]
usages = [
	"GL_STREAM_DRAW", "GL_STATIC_DRAW", "GL_DYNAMIC_DRAW",
	"GL_STREAM_READ", "GL_STATIC_READ", "GL_DYNAMIC_READ",
	"GL_STREAM_COPY", "GL_STATIC_COPY", "GL_DYNAMIC_COPY",
]
datatypes = [
	"GL_BYTE", "GL_UNSIGNED_BYTE",
	"GL_SHORT", "GL_UNSIGNED_SHORT",
	"GL_INT", "GL_UNSIGNED_INT",
	"GL_FLOAT", "GL_DOUBLE",
]
datatypes_format = "bBhHiIfd"

current_vbo = None
vbo_offset = 256
vbo_length = 0

out = open(1, "wb")
out.write(b"H3D\x7F")
pad = vbo_offset - 4
data = b""

for l in sys.stdin:
	l = l.strip()
	if not l or l[0] == "*":
		continue
	if l.startswith("vbos "):
		out.write(struct.pack("<B", int(l[5:])) + b"\0")
		pad -= 2
	elif l.startswith("vbo "):
		l = l[4:].split()
		normalized = False
		if l[0] == "normalized":
			normalized = True
			l = l[1:]
		t = types.index(l[0])
		u = usages.index(l[1])
		d = datatypes.index(l[2]) if len(l) > 2 else datatypes.index("GL_UNSIGNED_SHORT")
		if normalized:
			d |= 0x80
		r = int(l[3]) if len(l) > 3 else 1
		s = int(l[4]) if len(l) > 4 else 0
		current_vbo = (t, u, d, r, s)
	elif l == "end":
		if current_vbo is None:
			sys.stderr.write("outside of vbo\n")
			exit(1)
		out.write(struct.pack("<BBBBHII", *current_vbo, vbo_offset, vbo_length))
		pad -= 14
		current_vbo = None
		vbo_offset += vbo_length
		vbo_length = 0
	else:
		l = l.split()
		for x in l:
			x = eval(x, math.__dict__)
			if current_vbo is None:
				sys.stderr.write("outside of vbo\n")
				exit(1)
			else:
				d = struct.pack("<%s" % datatypes_format[current_vbo[2] & 0x7F], x)
				vbo_length += len(d)
				data += d

out.write(b"\0" * pad)
out.write(data)
